# DC Universe

DC Universe was made for Telerik's Java+Android first Android project assignment.  
It serves as a demonstration for the things we've learned in the Android module.

# Yeah, okay, but what does the application do?

The application itself is an encyclopedia for DC superheroes. It has entries (superheroes), each entry has some fields (including a picture).  
There are plenty of features to help with navigation/operation with those entries too. And you can create your own heroes :)

# Cool! But what exactly are those "features" that you talk about?

I won't dwell on that in the readme - Everything is demonstrated in the [presentation.](https://gitlab.com/nekr0s/dc-universe/tree/master/presentation) There is .pdf and .pptx for that.  
In addition - a [Class diagram.](https://gitlab.com/nekr0s/dc-universe/tree/master/diagram)

# What dependencies did you use?

I've used: 
* [Google's Firebase](https://firebase.google.com/)
* [hdodenhof's CircleImageView](https://github.com/hdodenhof/CircleImageView) 
* [Picasso](http://square.github.io/picasso/)