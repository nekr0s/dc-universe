package alexander.superheroes.views.list;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.SearchView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;

import alexander.superheroes.R;
import alexander.superheroes.models.HeroPlusImage;
import alexander.superheroes.models.Superhero;
import alexander.superheroes.views.about.AboutActivity;
import alexander.superheroes.views.create.SuperheroCreateActivity;
import alexander.superheroes.views.dialogs.RenameDialog;

import static alexander.superheroes.models.Superhero.generateId;

public class SuperheroesListActivity extends AppCompatActivity implements
        RenameDialog.RenameDialogListener, android.support.v7.widget.SearchView.OnQueryTextListener {
    private static final int GET_NEW_HERO = 555;
    private static final int DELETE_THIS_HERO = 666;
    private static final int UNSTAR_THIS_HERO = 123;
    private SuperheroesListFragment mSuperheroesListFragment;
    private String mWhichCollection = "superheros";
    private CoordinatorLayout coordinatorLayout;
    private boolean doubleBackToExitPressedOnce = false;
    private Handler mHandler = new Handler();
    private DrawerLayout mDrawerLayout;

    private final Runnable mRunnable = () -> doubleBackToExitPressedOnce = false;
    private String TAG = "APP/Superheroes";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_superheroes_list);


        // Sets up the ActionBar
        Toolbar toolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);

        // Sets up the Drawer
        mDrawerLayout = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                item -> {
                    item.setChecked(true);
                    mDrawerLayout.closeDrawers();
                    // Swap fragments
                    swapFragments(item);
                    return false;
                }
        );

        // Sets up coord
        coordinatorLayout = findViewById(R.id.coordinator_layout);

        // Sets up FAB
        FloatingActionButton button = findViewById(R.id.switch_button);

        // Load or create fragment
        if (savedInstanceState == null) {
            createFragment();
        } else {
            loadFragment();
        }


        button.setOnClickListener(v -> {
            // This button has to open up SuperheroCreateActivity
            Intent intent = new Intent(
                    this,
                    SuperheroCreateActivity.class
            );
            startActivityForResult(intent, GET_NEW_HERO);
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        SuperheroesListFragment frag = (SuperheroesListFragment) getSupportFragmentManager()
                .findFragmentByTag("my_fragment");

        if (requestCode == GET_NEW_HERO) {
            if (resultCode == Activity.RESULT_OK) {
                Superhero heroToAdd = data.getParcelableExtra("hero");
                String url = data.getStringExtra("url");
                frag.addToAdapter(heroToAdd, url);
            }
        } else if (requestCode == DELETE_THIS_HERO) {
            if (resultCode == Activity.RESULT_OK) {
                // position is needed for deletion at adapter, hero + url for data set entry
                int pos = data.getIntExtra("position", -1);
                Superhero hero = data.getParcelableExtra("hero");
                String url = data.getStringExtra("url");
                HeroPlusImage heroPlusImage = new HeroPlusImage(hero, url);
                // Deletes the hero from recycler view
                mSuperheroesListFragment.getAdapter().removeAt(pos);
                // Show snack bar
                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout, hero.name + " removed from list!", Snackbar.LENGTH_LONG);
                snackbar.setAction("UNDO",
                        view -> mSuperheroesListFragment.getAdapter().add(heroPlusImage, pos));
                snackbar.setActionTextColor(Color.WHITE);
                // If the bar is dismissed by a timeout || swipe -> safe to delete the hero from the db
                snackbar.addCallback(new Snackbar.Callback() {
                    @Override
                    public void onDismissed(Snackbar transientBottomBar, int event) {
                        if (event == DISMISS_EVENT_TIMEOUT || event == DISMISS_EVENT_SWIPE) {
                            frag.deleteFromDb(hero.name, generateId(hero.name));
                        } else {
                            super.onDismissed(transientBottomBar, event);
                        }
                    }
                });
                snackbar.show();
            } else if (resultCode == UNSTAR_THIS_HERO) {
                if (mWhichCollection.equals("favorites")) {
                    int pos = data.getIntExtra("position", -1);
                    frag.getAdapter().removeAt(pos);
                }
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("which_collection", mWhichCollection);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mWhichCollection = savedInstanceState.getString("which_collection");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mHandler != null) mHandler.removeCallbacks(mRunnable);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.list_toolbar_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Tap BACK button one more time to exit", Toast.LENGTH_SHORT).show();

        mHandler.postDelayed(mRunnable, 2000);
    }

    @Override
    public void applyNewName(String oldName, String newName, int position) {
        SuperheroesListFragment listFragment = (SuperheroesListFragment) getSupportFragmentManager()
                .findFragmentByTag("my_fragment");
        listFragment.rename(oldName, newName, position);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        // Try to replace with Streaming API
        // Adding new hero probably won't catch it, cause I'm not adding him to the initDataSet
        String userInput = s.toLowerCase();
        List<HeroPlusImage> newDataSet = new ArrayList<>();
        List<HeroPlusImage> initDataSet = mSuperheroesListFragment.getInitDataSet();
        for (HeroPlusImage data : initDataSet) {
            if (data.getHero().name.toLowerCase().contains(userInput) ||
                    data.getHero().secretIdentity.toLowerCase().contains(userInput)) {
                newDataSet.add(data);
            }
        }
        mSuperheroesListFragment.getAdapter().updateData(newDataSet);
        return true;
    }

    private void createFragment() {
        mSuperheroesListFragment = SuperheroesListFragment.newInstance();
        mSuperheroesListFragment.setCollection(mWhichCollection);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content, mSuperheroesListFragment, "my_fragment")
                .commit();
        if (mWhichCollection.equals("favorites")) {
            setTitle("Favorites");
        } else {
            setTitle(R.string.app_name);
        }
    }

    private void loadFragment() {
        mSuperheroesListFragment = (SuperheroesListFragment) getSupportFragmentManager()
                .findFragmentByTag("my_fragment");
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content, mSuperheroesListFragment)
                .commit();
    }

    private void swapFragments(MenuItem item) {
        if (item.getItemId() == R.id.good_guys
                && !mWhichCollection.equals("superheros")) {
            Log.d(TAG, "onCreate: Good guys | " + mWhichCollection);
            mWhichCollection = "superheros";
            createFragment();
        } else if (item.getItemId() == R.id.bad_guys
                && !mWhichCollection.equals("villains")) {
            Log.d(TAG, "onCreate: Bad guys | " + mWhichCollection);
            mWhichCollection = "villains";
            createFragment();
        } else if (item.getItemId() == R.id.nav_favorites
                && !mWhichCollection.equals("favorites")) {
            Log.d(TAG, "onCreate: Favorite guys | " + mWhichCollection);
            mWhichCollection = "favorites";
            createFragment();
        } else if (item.getItemId() == R.id.nav_about) {

            Intent intent = new Intent(
                    this,
                    AboutActivity.class
            );
            // Starts AboutActivity
            mWhichCollection = "about";
            startActivity(intent);
        }
    }

}