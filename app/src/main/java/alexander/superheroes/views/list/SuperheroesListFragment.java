package alexander.superheroes.views.list;

import android.app.AlertDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import alexander.superheroes.R;
import alexander.superheroes.models.HeroPlusImage;
import alexander.superheroes.models.Superhero;
import alexander.superheroes.views.dialogs.RenameDialog;
import alexander.superheroes.views.recycler.RecyclerViewAdapter;

import static alexander.superheroes.models.Superhero.generateId;

/**
 * A simple {@link Fragment} subclass.
 */

public class SuperheroesListFragment extends android.support.v4.app.Fragment {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private FirebaseFirestore mDb;
    private StorageReference mStorageRef;
    private String mWhichCollection;
    private RecyclerViewAdapter mAdapter;
    private List<HeroPlusImage> mDataSet = new ArrayList<>();

    public SuperheroesListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view;

        // Gets previous SIDE of the character, if any
        if (savedInstanceState != null)
            mWhichCollection = savedInstanceState.getString("which_collection");

        // Sets vision of fragment, depending on which side the character is on
        if (mWhichCollection.equals("superheros") || mWhichCollection.equals("favorites")) {
            view = inflater.inflate(R.layout.fragment_superheroes_list, container, false);
        } else {
            view = inflater.inflate(R.layout.fragment_superheroes_list_night, container, false);
        }

        // Sets the refresher
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_refresh);
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            if (mAdapter != null && mAdapter.getImagesCount() == mAdapter.getDataSet().size()) {
                mAdapter.clear();
                initCircleImages(view);
                initRecyclerView(view);
            }
            mSwipeRefreshLayout.setRefreshing(false);
        });

        // Sets the RecyclerView
        if (savedInstanceState == null) {
            initCircleImages(view);
            initRecyclerView(view);
        } else {
            initRecyclerView(view);
            mAdapter.setImagesCount(mDataSet.size());
        }

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mStorageRef = FirebaseStorage.getInstance().getReference();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("which_collection", mWhichCollection);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int position = mAdapter.getPosition();
        String name = mAdapter.getDataSet().get(position).getHero().name;
        String id = generateId(name);

        switch (item.getItemId()) {
            case R.id.opt_delete:
                deleteConfirmationWindow(name, id, position);
                return true;
            case R.id.opt_edit:
                Toast.makeText(getContext(), "Edit", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.opt_rename:
                openRenameDialog(name, position);
                return true;
            case R.id.opt_remove_favorite:
                removeFavorite(id, position);
                return true;
        }
        return super.onContextItemSelected(item);

    }

    public static SuperheroesListFragment newInstance() {
        return new SuperheroesListFragment();
    }

    public void setCollection(String collection) {
        mWhichCollection = collection;
    }

    public void addToAdapter(Superhero hero, String url) {
        mAdapter.add(new HeroPlusImage(hero, url));
    }

    public void rename(String oldName, String newName, int position) {
        if (oldName.equals(newName)) {
            return;
        }
        mDb.collection(mWhichCollection)
                .document(generateId(oldName))
                .get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Map<String, Object> doc = task.getResult().getData();
                doc.put("name", newName);
                // Deletes old entry -> puts updated entry with new id and name
                String generatedOldId = generateId(oldName);
                String generateNewId = generateId(newName);
                mDb.collection(mWhichCollection).document(generatedOldId).delete();
                mDb.collection(mWhichCollection).document(generateNewId).set(doc)
                        .addOnCompleteListener(task2 -> {
                            reupload(oldName, newName);
                            mDataSet.get(position).getHero().setName(newName);
                            mAdapter.notifyItemChanged(position);
                        });
                // Do that with favorites entry, if it exists
                mDb.collection("favorites").document(generatedOldId).get()
                        .addOnSuccessListener(task2 -> {
                            if (task2.exists()) {
                                mDb.collection("favorites").document(generatedOldId).delete();
                                mDb.collection("favorites").document(generateNewId).set(doc);
                            }
                        });
            }
        });
    }

    public void deleteFromDb(String name, String id) {
        mDb.collection(mWhichCollection).document(id).delete()
                .addOnSuccessListener(task -> {
                    if (mWhichCollection.equals("favorites"))
                        return;
                    // Also deletes picture from storage
                    mStorageRef.child("images/" + name + ".png").delete();
                    // Also delete from favorites
                    mDb.collection("favorites").document(id).get()
                            .addOnSuccessListener(documentSnapshot -> {
                                if (documentSnapshot.exists())
                                    mDb.collection("favorites").document(id).delete();
                            });
                });
    }

    // Both getters are used in Search option
    public RecyclerViewAdapter getAdapter() {
        return mAdapter;
    }

    public List<HeroPlusImage> getInitDataSet() {
        return mDataSet;
    }

    private void initCircleImages(View view) {
        mDb = FirebaseFirestore.getInstance();
        mDb.collection(mWhichCollection)
                .get()
                .addOnCompleteListener(task -> {
                    List<Superhero> superheroes = task.getResult()
                            .toObjects(Superhero.class);

                    for (int i = 0; i < superheroes.size(); i++) {
                        mDataSet.add(new HeroPlusImage(superheroes.get(i)));
                        int finalI = i;
                        mStorageRef.child("images/" + superheroes.get(i).name + ".png").getDownloadUrl()
                                .addOnCompleteListener(imageTask -> {
                                    mAdapter.updateImage(imageTask.getResult().toString(), finalI);
                                });
                    }
                });
    }

    private void initRecyclerView(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.rv_superheroes);
        mAdapter = new RecyclerViewAdapter(mDataSet, mWhichCollection, getContext());
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
    }

    private void removeFavorite(String id, int position) {
        mDb.collection(mWhichCollection).document(id).delete().addOnCompleteListener(task -> {
            Toast.makeText(getContext(), "Hero removed from favorites!",
                    Toast.LENGTH_SHORT).show();
            mAdapter.removeAt(position);
        });
    }

    private void openRenameDialog(String oldName, int position) {
        RenameDialog renameDialog = new RenameDialog();
        Bundle args = new Bundle();
        args.putInt("position", position);
        args.putString("old_name", oldName);
        renameDialog.setArguments(args);

        renameDialog.show(getFragmentManager(), "rename_dialog");
    }

    private void deleteConfirmationWindow(String name, String id, int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.are_you_sure);
        builder.setMessage(R.string.deletion_explain);
        builder.setPositiveButton("DELETE", (dialogInterface, i) -> {
            deleteFromDb(name, id);
            mAdapter.removeAt(position);
        });
        builder.setNegativeButton(R.string.cancel,
                (dialogInterface, i) -> dialogInterface.dismiss());

        AlertDialog alertDialog = builder.create();
        // Sets delete button to red have red color
        alertDialog.setOnShowListener(listener ->
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.RED));
        alertDialog.show();
    }

    private void reupload(String oldName, String newName) {
        mStorageRef.child("images/" + oldName + ".png")
                .getStream()
                .addOnSuccessListener(taskSnapshot -> mStorageRef.child("images/" + newName + ".png")
                        .putStream(taskSnapshot.getStream()).addOnSuccessListener(task -> mStorageRef
                                .child("images/" + oldName + ".png").delete()));


    }
}

