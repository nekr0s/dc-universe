package alexander.superheroes.views.create;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;

import alexander.superheroes.R;
import alexander.superheroes.models.Superhero;

import static alexander.superheroes.models.Superhero.generateId;
import static android.app.Activity.RESULT_OK;

public class SuperheroCreateFragment extends android.support.v4.app.Fragment
        implements AdapterView.OnItemSelectedListener {

    private EditText mEditTextName;
    private TextView mTextViewImage;
    private Button mUploadButton;
    private EditText mEditTextSecretId;
    private EditText mEditTextShortStory;
    private Spinner mDropDown;
    private String mWhichSide;
    private final FirebaseFirestore database = FirebaseFirestore.getInstance();

    private Superhero superheroToUpload;

    private final int PICK_IMAGE_REQUEST = 1;
    private Uri mImageUri;
    private UploadTask uploadTask;
    private ArrayAdapter<CharSequence> mAdapter;
    private ProgressBar mSpinner;

    public SuperheroCreateFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_superhero_create, container, false);

        mEditTextName = view.findViewById(R.id.hero_name);
        mTextViewImage = view.findViewById(R.id.image_name);
        mUploadButton = view.findViewById(R.id.upload_button);
        mEditTextSecretId = view.findViewById(R.id.secret_id_upload);
        mEditTextShortStory = view.findViewById(R.id.short_story_upload);
        mSpinner = view.findViewById(R.id.progressBar);
        mSpinner.setVisibility(View.GONE);

        // Set up the Dropdown
        mDropDown = view.findViewById(R.id.spinner);
        mAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.dropdown_options, android.R.layout.simple_spinner_item);
        mAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        mDropDown.setAdapter(mAdapter);
        mDropDown.setOnItemSelectedListener(this);

        mUploadButton.setOnClickListener(click -> openFileChooser());

        return view;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        mWhichSide = mAdapter.getItem(i).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        mWhichSide = "";
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            mImageUri = data.getData();
            mTextViewImage.setText(data.getData().getLastPathSegment());

        }

    }

    public void upload() {
        if (mImageUri == null || TextUtils.isEmpty(mEditTextName.getText())
                || TextUtils.isEmpty(mEditTextShortStory.getText())
                || TextUtils.isEmpty(mEditTextSecretId.getText())) {
            Toast.makeText(getContext(), "Check all fields!", Toast.LENGTH_SHORT).show();
            return;
        }
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference imageRef = storage.getReference()
                .child("images/" + mEditTextName.getText() + ".png");
        // Uploads image
        mSpinner.setVisibility(View.VISIBLE);
        uploadTask = imageRef.putFile(mImageUri);
        // Uploads Superhero
        Superhero h = uploadSuperhero(mWhichSide.toLowerCase().concat("s"));

        uploadTask.addOnSuccessListener(task -> {
            mSpinner.setVisibility(View.GONE);
            Toast.makeText(getContext(),
                    "Uploaded!", Toast.LENGTH_SHORT)
                    .show();
            ((SuperheroCreateActivity) getActivity()).sendIntent(h, task.getDownloadUrl().toString());
        });
    }

    private void openFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    private void setSuperheroToUpload(String name, String secretIdentity, String shortStory) {
        superheroToUpload = new Superhero(name, secretIdentity, shortStory);
    }

    private Superhero uploadSuperhero(String whichSide) {
        setSuperheroToUpload(mEditTextName.getText().toString(),
                mEditTextSecretId.getText().toString(),
                mEditTextShortStory.getText().toString());
        String id = generateId(superheroToUpload.name);
        database.collection(whichSide)
                .document(id).set(superheroToUpload);
        // On success send some DATA indicating CHANGES
        return superheroToUpload;
    }

}
