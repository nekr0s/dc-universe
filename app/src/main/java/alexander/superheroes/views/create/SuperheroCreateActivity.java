package alexander.superheroes.views.create;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;

import alexander.superheroes.R;
import alexander.superheroes.models.Superhero;

public class SuperheroCreateActivity extends AppCompatActivity {

    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_superhero_create);
        // Sets up the ActionBar
        mToolbar = findViewById(R.id.create_superhero_toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Create Hero");

        // Dismiss Action
        mToolbar.setNavigationIcon(R.drawable.ic_discard);
        mToolbar.setNavigationOnClickListener(view -> {
            // Do something
            finish();
        });

        // Submit Action
        mToolbar.setOnMenuItemClickListener(item -> {

            if (item.getItemId() == R.id.superhero_submit) {
                // Do something
                SuperheroCreateFragment mCreator = (SuperheroCreateFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.create_fragment);
                mCreator.upload();
            }
            return true;
        });
    }

    public void sendIntent(Superhero hero, String url) {
        if (hero == null) {
            Intent intent = new Intent();
            setResult(Activity.RESULT_CANCELED, intent);
            finish();
        } else {
            Intent intent = new Intent();
            intent.putExtra("hero", hero);
            intent.putExtra("url", url);
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.create_toolbar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
