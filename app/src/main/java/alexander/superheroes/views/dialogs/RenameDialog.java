package alexander.superheroes.views.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import alexander.superheroes.R;

public class RenameDialog extends AppCompatDialogFragment {

    private EditText mNewName;
    private RenameDialogListener mListener;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();
        int position = args.getInt("position");
        String oldName = args.getString("old_name");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_rename, null);

        builder.setView(view)
                .setNegativeButton(R.string.cancel,
                        (dialogInterface, i) -> dialogInterface.dismiss())
                .setPositiveButton(R.string.save, (dialogInterface, i) -> {
                    String newName = mNewName.getText().toString();
                    mListener.applyNewName(oldName, newName, position);
                });

        mNewName = view.findViewById(R.id.new_name);
        mNewName.setText(oldName);
        mNewName.setSelection(0, oldName.length());
        mNewName.requestFocus();

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (RenameDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement RenameDialogListener");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    public interface RenameDialogListener {
        void applyNewName(String oldName, String newName, int position);
    }

}
