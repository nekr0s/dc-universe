package alexander.superheroes.views.details;

import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import alexander.superheroes.R;
import alexander.superheroes.models.Superhero;

import static alexander.superheroes.models.Superhero.generateId;

/**
 * A simple {@link Fragment} subclass.
 */
public class SuperheroDetailFragment extends android.support.v4.app.Fragment {

    Superhero superhero;
    private int mPositionInAdapter;
    private TextView mSecretIdentityTextView;
    private TextView mShortStoryTextView;
    private CollectionReference mFavoritesDb;
    private boolean mIsHeroInFavorites = false;
    public String mUrl;

    public SuperheroDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_superhero_detail, container, false);
        ImageView mPortraitImageView = view.findViewById(R.id.portrait);
        mSecretIdentityTextView = view.findViewById(R.id.secret_id);
        mShortStoryTextView = view.findViewById(R.id.story);

        // Loads images
        if (savedInstanceState != null) {
            Picasso.get().load(savedInstanceState.getString("image_url"))
                    .placeholder(R.drawable.common_full_open_on_phone)
                    .into(mPortraitImageView);
        } else {
            Picasso.get().load(mUrl)
                    .placeholder(R.drawable.common_full_open_on_phone)
                    .into(mPortraitImageView);
        }

        mFavoritesDb = FirebaseFirestore.getInstance().collection("favorites");

//      Create superhero map to add to db
        Map<String, Object> hero = new HashMap<>();
        hero.put("name", superhero.name);
        hero.put("secretIdentity", superhero.secretIdentity);
        hero.put("shortStory", superhero.shortStory);

        // superheros+batman, villains+the+joker
        String id = generateId(superhero.name);


        // Add favorites button
        FloatingActionButton favButton = view.findViewById(R.id.fav_button);
        checkIfFavoriteExists(id, mFavoritesDb, favButton);

        // Adds onClick logic
        favButton.setOnClickListener(v -> starButtonLogic(hero, id, favButton));


        mSecretIdentityTextView.setText(superhero.secretIdentity);
        mShortStoryTextView.setText(superhero.shortStory);

        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("image_url", mUrl);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public static SuperheroDetailFragment newInstance() {
        return new SuperheroDetailFragment();
    }

    public void setPositionInAdapter(int position) {
        mPositionInAdapter = position;
    }

    public void setSuperhero(String name, String secretId, String shortStory, String url) {
        this.mUrl = url;
        superhero = new Superhero(name, secretId, shortStory);
        if (mSecretIdentityTextView == null)
            return;
        mSecretIdentityTextView.setText(secretId);
        mShortStoryTextView.setText(shortStory);
    }

    private void setStar(FloatingActionButton starButton, boolean bool) {
        if (bool) {
            starButton.setImageResource(R.drawable.ic_star_yellow);
            mIsHeroInFavorites = true;
        } else {
            starButton.setImageResource(R.drawable.ic_star);
            mIsHeroInFavorites = false;
        }
    }

    private void checkIfFavoriteExists(String heroToCheck, CollectionReference collection,
                                       FloatingActionButton fab) {
        DocumentReference docRef = collection.document(heroToCheck);
        docRef.get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            Log.d("ASDF", "DocumentSnapshot data FOUND: " + document.getData());
                            setStar(fab, true);
                        } else {
                            Log.d("ASDF", "Not found");
                            setStar(fab, false);
                        }
                    } else {
                        Log.d("ASDF", "get failed with ", task.getException());
                    }
                });
        return;
    }

    private void starButtonLogic(Map<String, Object> hero, String id,
                                 FloatingActionButton favButton) {
        if (mIsHeroInFavorites) {
            // Remove current hero from favorites
            Intent intent = new Intent();
            intent.putExtra("position", mPositionInAdapter);
            getActivity().setResult(123, intent);
            mFavoritesDb.document(id).delete();
            mIsHeroInFavorites = false;
            favButton.setImageResource(R.drawable.ic_star);
            Toast.makeText(getContext(), "Hero removed from favorites.",
                    Toast.LENGTH_SHORT).show();

        } else {
            // Add current hero to favorites
            mFavoritesDb.document(id).set(hero);
            mIsHeroInFavorites = true;
            favButton.setImageResource(R.drawable.ic_star_yellow);
            Toast.makeText(getContext(), "Hero added to favorites!",
                    Toast.LENGTH_SHORT).show();
        }
    }
}
