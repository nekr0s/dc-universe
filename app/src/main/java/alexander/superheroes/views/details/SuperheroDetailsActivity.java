package alexander.superheroes.views.details;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.Toast;

import alexander.superheroes.R;

public class SuperheroDetailsActivity extends AppCompatActivity {


    private int mPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_superhero_details);

        String name, id, story, url;
        SuperheroDetailFragment mSuperheroDetailsFragment;

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            name = intent.getStringExtra("SUPERHERO_NAME");
            id = intent.getStringExtra("SECRET_ID");
            story = intent.getStringExtra("STORY");
            url = intent.getStringExtra("URL");
            mPosition = intent.getIntExtra("POSITION", -1);

            mSuperheroDetailsFragment = SuperheroDetailFragment.newInstance();

        } else {
            mSuperheroDetailsFragment = (SuperheroDetailFragment) getSupportFragmentManager()
                    .findFragmentByTag("current_details");
            name = mSuperheroDetailsFragment.superhero.name;
            id = mSuperheroDetailsFragment.superhero.secretIdentity;
            story = mSuperheroDetailsFragment.superhero.shortStory;
            url = mSuperheroDetailsFragment.mUrl;
        }

        mSuperheroDetailsFragment.setSuperhero(name, id, story, url);
        mSuperheroDetailsFragment.setPositionInAdapter(mPosition);

        // Set up toolbar
        Toolbar toolbar = findViewById(R.id.superhero_details_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(name);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(view -> finish());

        int finalPosition = mPosition;
        toolbar.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.superhero_delete) {
                // Delete superhero
                Intent intent = new Intent();
                intent.putExtra("hero", mSuperheroDetailsFragment.superhero);
                intent.putExtra("url", url);
                intent.putExtra("position", finalPosition);
                setResult(Activity.RESULT_OK, intent);
                finish();
            } else if (item.getItemId() == R.id.superhero_edit) {
                // Edit superhero
                Toast.makeText(this, "Edit", Toast.LENGTH_SHORT).show();
            }
            return true;
        });


        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content, mSuperheroDetailsFragment, "current_details")
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.details_toolbar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

}