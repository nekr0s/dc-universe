package alexander.superheroes.views.recycler;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import alexander.superheroes.R;
import alexander.superheroes.models.HeroPlusImage;
import alexander.superheroes.views.details.SuperheroDetailsActivity;
import alexander.superheroes.views.list.SuperheroesListActivity;
import de.hdodenhof.circleimageview.CircleImageView;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private static final String TAG = "RecyclerViewAdapter";
    private static final int DELETE_THIS_HERO = 666;
    private List<HeroPlusImage> mDataSet;
    private String mWhichSide;
    private Context mContext;

    private int position;
    private int imagesCount = 0;

    public RecyclerViewAdapter(List<HeroPlusImage> dataSet, String mWhichSide, Context mContext) {
        this.mDataSet = dataSet;
        this.mContext = mContext;
        this.mWhichSide = mWhichSide;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitem,
                parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        if (mWhichSide.equals("villains")) {
            viewHolder.imageName.setTextColor(Color.WHITE);
            viewHolder.imageView.setBorderColor(Color.parseColor("#212121"));
            viewHolder.imageName.setShadowLayer(2, 10, 5, Color.parseColor("#212121"));
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: called.");
        Picasso.get().load(mDataSet.get(position).getImageUrl()).into(holder.imageView);

        holder.imageName.setText(mDataSet.get(position).getHero().name);

        holder.parentLayout.setOnClickListener(view -> {
            Intent intent = new Intent(mContext, SuperheroDetailsActivity.class);
            intent.putExtra("SUPERHERO_NAME", mDataSet
                    .get(position).getHero().name);
            intent.putExtra("SECRET_ID", mDataSet.get(position).getHero().secretIdentity);
            intent.putExtra("STORY", mDataSet.get(position).getHero().shortStory);
            intent.putExtra("URL", mDataSet.get(position).getImageUrl());
            intent.putExtra("POSITION", position);
            ((SuperheroesListActivity) mContext).startActivityForResult(intent, DELETE_THIS_HERO);
        });
        holder.parentLayout.setOnLongClickListener(view -> {
            setPosition(holder.getAdapterPosition());
            return false;
        });
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public int getPosition() {
        return position;
    }

    public int getImagesCount() {
        return imagesCount;
    }

    public List<HeroPlusImage> getDataSet() {
        return mDataSet;
    }

    public void setImagesCount(int imagesCount) {
        this.imagesCount = imagesCount;
    }

    public void add(HeroPlusImage heroPlusImage) {
        mDataSet.add(heroPlusImage);
        notifyItemInserted(getItemCount() - 1);
    }

    public void add(HeroPlusImage heroPlusImage, int position) {
        mDataSet.add(position, heroPlusImage);
        notifyItemInserted(position);
        notifyItemRangeChanged(position, mDataSet.size());
    }

    public void removeAt(int position) {
        mDataSet.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mDataSet.size());
    }

    public void updateImage(String s, int finalI) {
        imagesCount++;
        mDataSet.get(finalI).setImageUrl(s);
        notifyItemChanged(finalI);
    }

    public void clear() {
        mDataSet.clear();
        notifyDataSetChanged();
    }

    public void updateData(List<HeroPlusImage> newDataSet) {
        mDataSet = new ArrayList<>();
        mDataSet.addAll(newDataSet);
        notifyDataSetChanged();
    }

    private void setPosition(int position) {
        this.position = position;
    }

    private String getWhichSide() {
        return mWhichSide;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        CircleImageView imageView;
        TextView imageName;
        RelativeLayout parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.circle_image);
            imageName = itemView.findViewById(R.id.image_name);
            parentLayout = itemView.findViewById(R.id.parent_layout);
            itemView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View view,
                                        ContextMenu.ContextMenuInfo contextMenuInfo) {
            menu.add(Menu.NONE, R.id.opt_edit, Menu.NONE, "Edit");
            if (!getWhichSide().equals("favorites")) {
                menu.add(Menu.NONE, R.id.opt_rename, Menu.NONE, "Rename");
                menu.add(Menu.NONE, R.id.opt_delete, Menu.NONE, "Delete");
            } else {
                menu.add(Menu.NONE, R.id.opt_remove_favorite, Menu.NONE,
                        "Remove hero from favorites");
            }
        }

    }
}
