package alexander.superheroes.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Superhero implements Parcelable {
    public String name;
    public String secretIdentity;
    public String shortStory;

    private Superhero(Parcel in) {
        name = in.readString();
        secretIdentity = in.readString();
        shortStory = in.readString();
    }

    public static final Creator<Superhero> CREATOR = new Creator<Superhero>() {
        @Override
        public Superhero createFromParcel(Parcel in) {
            return new Superhero(in);
        }

        @Override
        public Superhero[] newArray(int size) {
            return new Superhero[size];
        }
    };

    public Superhero() {
        // keep empty
    }

    public Superhero(String n, String id, String st) {
        name = n;
        secretIdentity = id;
        shortStory = st;
    }

    public static String generateId(String name) {
        return name.toLowerCase().replace(' ', '+')
                .replace('-', '+');
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(secretIdentity);
        parcel.writeString(shortStory);
    }

    @Override
    public String toString() {
        return name;
    }

}
