package alexander.superheroes.models;

public class HeroPlusImage {
    private Superhero hero;
    private String imageUrl;

    public HeroPlusImage(Superhero hero) {
        this.hero = hero;
    }

    public HeroPlusImage(Superhero hero, String url) {
        this.hero = hero;
        this.imageUrl = url;
    }

    @Override
    public String toString() {
        return hero.name;
    }

    public Superhero getHero() {
        return hero;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setHero(Superhero hero) {
        this.hero = hero;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;

    }
}